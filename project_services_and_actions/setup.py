from setuptools import setup
import os
from glob import glob

package_name = 'project_services_and_actions'

setup(
    name=package_name,
    version='0.0.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
        (os.path.join('share', package_name, 'launch'), glob('launch/*.launch.py'))
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='user',
    maintainer_email='duckfrost@gmail.com',
    description='TODO: Package description',
    license='TODO: License declaration',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'rb1_move_to_pick_location_service = project_services_and_actions.service_rb1_move:main',
            'rb1_move_to_pick_location_action = project_services_and_actions.action_rb1_move:main',
            'action_client_test_exe = project_services_and_actions.action_client_test:main',
            'action_server_test_exe = project_services_and_actions.action_server_test:main',
            'action_server_v2_exe = project_services_and_actions.action_server_v2:main'
        ],
    },
)
