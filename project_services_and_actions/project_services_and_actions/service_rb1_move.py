import rclpy
import numpy as np
import time
# import the ROS2 python libraries
from rclpy.node import Node
# import the Twist module from geometry_msgs interface
from geometry_msgs.msg import Twist
# import the LaserScan module from sensor_msgs interface
from sensor_msgs.msg import LaserScan
from nav_msgs.msg import Odometry

from rclpy.qos import ReliabilityPolicy, QoSProfile
from rclpy.callback_groups import ReentrantCallbackGroup, MutuallyExclusiveCallbackGroup
from rclpy.executors import MultiThreadedExecutor

from project_custom_interfaces.srv import GoToLoading

from std_msgs.msg import Empty


class RB1Movement(Node):

    def __init__(self, min_dist_wall=0.3, loop_period=0.1):
        # Here we have the class constructor
        # call the class constructor
        super().__init__('subs_scan_pub_cmd')
        self._loop_period = loop_period
        self._min_dist_wall = min_dist_wall
        self.init_movement_state()

        self.group1 = ReentrantCallbackGroup()

        # create the publisher object
        self.vel_pub = self.create_publisher(
            Twist,
            '/robot/cmd_vel',
            1)
        # Create publisher to elevator
        self.elevator_pub = self.create_publisher(Empty, '/elevator_up', 1)

        # create the subscriber object
        self.scan_sub = self.create_subscription(
            LaserScan,
            '/scan',
            self.scan_callback,
            QoSProfile(
                depth=1, reliability=ReliabilityPolicy.BEST_EFFORT),
            callback_group=self.group1)

        self.odom_sub = self.create_subscription(
            Odometry,
            '/odom',
            self.odom_callback,
            1,
            callback_group=self.group1)

        self.srv = self.create_service(
            GoToLoading, '/moving', self.StartMove_callback, callback_group=self.group1)

        self.get_logger().warning("Movement Service Server READY!...")

    def elevator_up(self):
        self.up = Empty()
        self.elevator_pub.publish(self.up)

    def init_movement_state(self):
        self.movement_state = "GOFOWARDS_1"
        self.init_turn_yaw = 0.0
        self.cmd = Twist()

    def scan_callback(self, msg):
        self.get_logger().debug("Scan CallBack")
        self.laser_msg = msg

    def get_front_laser(self):
        return self.laser_msg.ranges[int(len(self.laser_msg.ranges)/2)]

    def get_yaw(self):
        return self.yaw

    def euler_from_quaternion(self, quaternion):
        """
        Converts quaternion (w in last place) to euler roll, pitch, yaw
        quaternion = [x, y, z, w]
        Bellow should be replaced when porting for ROS 2 Python tf_conversions is done.
        """
        x = quaternion[0]
        y = quaternion[1]
        z = quaternion[2]
        w = quaternion[3]

        sinr_cosp = 2 * (w * x + y * z)
        cosr_cosp = 1 - 2 * (x * x + y * y)
        roll = np.arctan2(sinr_cosp, cosr_cosp)

        sinp = 2 * (w * y - z * x)
        pitch = np.arcsin(sinp)

        siny_cosp = 2 * (w * z + x * y)
        cosy_cosp = 1 - 2 * (y * y + z * z)
        yaw = np.arctan2(siny_cosp, cosy_cosp)

        return roll, pitch, yaw

    def odom_callback(self, msg):
        self.get_logger().debug("Odom CallBack")
        orientation_q = msg.pose.pose.orientation
        orientation_list = [orientation_q.x,
                            orientation_q.y, orientation_q.z, orientation_q.w]
        (self.roll, self.pitch, self.yaw) = self.euler_from_quaternion(orientation_list)

    def check_wall_detected(self):
        """
        We check if we are at less than max_dist of an object infront
        """
        result = self.get_front_laser() < self._min_dist_wall
        self.get_logger().info("check_wall_detected:"+str(result)+",dist=" +
                               str(self.get_front_laser())+",min="+str(self._min_dist_wall))
        return result

    def stop_robot(self, index):
        self.get_logger().info("STOP="+str(index))
        self.cmd.linear.x = 0.0
        self.cmd.angular.z = 0.0
        self.vel_pub.publish(self.cmd)

    def move_straight(self):
        self.get_logger().info("MOVE STRAIGHT")
        self.cmd.linear.x = 0.3
        self.cmd.angular.z = 0.0
        self.vel_pub.publish(self.cmd)

    def rotate(self, dir):
        self.get_logger().info("MOVE ROTATE")
        if dir == "left":
            self.cmd.angular.z = 0.2
        else:
            self.cmd.angular.z = -0.2
        self.cmd.linear.x = 0.0
        self.vel_pub.publish(self.cmd)

    def check_turn(self, desired_angle):
        """
        We chekc the init yaw with the current yaw , how much turn has been done
        we evaluate in abs angle
        """
        delta = self.init_turn_yaw - self.get_yaw()
        result = abs(delta) > abs(desired_angle)
        self.get_logger().info("Init YAW="+str(self.init_turn_yaw))
        self.get_logger().info("Current YAW="+str(self.get_yaw()))
        self.get_logger().info("Delta YAW="+str(self.init_turn_yaw))
        self.get_logger().info("RESULT YAW="+str(result))

        return result

    def StartMove_callback(self, request, response):
        self.get_logger().warning("Movement Service Server Message...")
        rate = self.create_rate(2)

        while self.movement_state != "END":
            self.move_loop()
            self.get_logger().info("Start sleep")
            rate.sleep()
            self.get_logger().info("End Sleep")

        response.success = True
        self.get_logger().warning("Movement Service Server Message...DONE")
        return response

    def move_loop(self):
        self.get_logger().info("Move Loop - STATE:"+str(self.movement_state))

        # We decide what to do based on state and scane and odom values:

        if self.movement_state == "START":
            # We check if teh distance is bigger than the min distance to wall
            if self.check_wall_detected():
                # We dont move or change state
                self.stop_robot(1)
            else:
                # We dont see wall, then go fowards
                self.movement_state = "GOFOWARDS_1"
        else:
            if self.movement_state == "GOFOWARDS_1":
                # We look for a wall
                if self.check_wall_detected():
                    # We stop We change to move right state
                    self.stop_robot(2)
                    self.movement_state = "TURN_RIGHT"
                    self.init_turn_yaw = self.get_yaw()
                else:
                    # We dont see wall, then go fowards
                    self.move_straight()
            else:
                # We are turning RIGHT
                if self.movement_state == "TURN_RIGHT":
                    # We have to check if we have turned 90 degrees ( 1.57 rad)
                    if self.check_turn(1.57):
                        self.stop_robot(3)
                        self.movement_state = "GOFOWARDS_2"
                    else:
                        # Continue turning
                        self.rotate("right")

                else:
                    if self.movement_state == "GOFOWARDS_2":
                        # We look for a wall
                        if self.check_wall_detected():
                            # We stop We change to move right state
                            self.stop_robot(4)
                            self.elevator_up()
                            self.movement_state = "END"
                        else:
                            # We dont see wall, then go fowards
                            self.move_straight()
                    else:
                        self.get_logger().info("ENDED MOVEMENT")

        self.get_logger().info("ENDED Loop")


def main(args=None):
    rclpy.init(args=args)
    try:
        rb1_mov_node = RB1Movement()

        executor = MultiThreadedExecutor(num_threads=3)
        executor.add_node(rb1_mov_node)

        try:
            executor.spin()
        finally:
            executor.shutdown()
            rb1_mov_node.destroy_node()

    finally:
        rclpy.shutdown()


if __name__ == '__main__':
    main()
