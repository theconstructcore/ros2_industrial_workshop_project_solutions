import rclpy
from rclpy.action import ActionServer
from rclpy.node import Node

from project_custom_interfaces.action import GoToLoadingWithF

from geometry_msgs.msg import Twist
import time

from rclpy.executors import MultiThreadedExecutor
from rclpy.callback_groups import ReentrantCallbackGroup, MutuallyExclusiveCallbackGroup
from rclpy.qos import ReliabilityPolicy, QoSProfile

from sensor_msgs.msg import LaserScan
from nav_msgs.msg import Odometry
import numpy as np

from std_msgs.msg import Empty
from geometry_msgs.msg import Twist


class MyActionServer(Node):

    def __init__(self, min_dist_wall=0.3, loop_period=0.1):
        super().__init__('my_action_server')

        # Doenst matter which Reentrant or mutual
        self.group_as = MutuallyExclusiveCallbackGroup()
        self.group_laser = MutuallyExclusiveCallbackGroup()
        self.group_odom = MutuallyExclusiveCallbackGroup()

        self._loop_period = loop_period
        self._min_dist_wall = min_dist_wall
        self.init_movement_state()

        self._action_server = ActionServer(
            self, GoToLoadingWithF, 'my_moving_robot_as', self.start_move_execute_callback, callback_group=self.group_as)

        self.get_logger().info('ACTION SERVER Object created...')

        # create the subscriber object
        self.scan_sub = self.create_subscription(
            LaserScan,
            '/scan',
            self.scan_callback,
            QoSProfile(
                depth=1, reliability=ReliabilityPolicy.BEST_EFFORT),
            callback_group=self.group_laser)

        self.odom_sub = self.create_subscription(
            Odometry,
            '/odom',
            self.odom_callback,
            1,
            callback_group=self.group_odom)

        # create the publisher object
        self.vel_pub = self.create_publisher(
            Twist,
            '/robot/cmd_vel',
            1)
        # Create publisher to elevator
        self.elevator_pub = self.create_publisher(Empty, '/elevator_up', 1)

        self.get_logger().info('SERVER ACTION READY...V6')

    def execute_callback(self, goal_handle):

        self.get_logger().info('Executing goal...')

        feedback_msg = GoToLoadingWithF.Feedback()
        feedback_msg.status = "Testing...."
        feedback_msg.distance_to_wall = 1.23456789

        for i in range(5):

            self.get_logger().info('Feedback: '+str(feedback_msg.status) +
                                   ",distance="+str(feedback_msg.distance_to_wall))

            goal_handle.publish_feedback(feedback_msg)

            time.sleep(1)

        goal_handle.succeed()

        feedback_msg.status = "ENDED...."
        feedback_msg.distance_to_wall = 0.0
        result = GoToLoadingWithF.Result()
        result.complete = True
        return result

    def start_move_execute_callback(self, goal_handle):

        input_state = goal_handle.request.init_state
        self.get_logger().warning('Executing Action goal in start state='+str(input_state))
        rate = self.create_rate(2)
        feedback_msg = GoToLoadingWithF.Feedback()

        while self.movement_state != "END":
            feedback_msg.status = self.movement_state
            feedback_msg.distance_to_wall = self.get_front_laser()
            goal_handle.publish_feedback(feedback_msg)
            self.move_loop()
            self.get_logger().info("Start sleep")
            rate.sleep()
            self.get_logger().info("End Sleep")

        goal_handle.succeed()

        result = GoToLoadingWithF.Result()
        result.complete = True
        return result

    def scan_callback(self, msg):
        self.laser_msg = msg
        self.get_logger().debug("Scan CallBack"+str(self.laser_msg.header))

    def odom_callback(self, msg):
        self.get_logger().debug("Odom CallBack")
        orientation_q = msg.pose.pose.orientation
        orientation_list = [orientation_q.x,
                            orientation_q.y, orientation_q.z, orientation_q.w]
        (self.roll, self.pitch, self.yaw) = self.euler_from_quaternion(orientation_list)
        self.get_logger().debug("Odom CallBack"+str(self.yaw))

    def euler_from_quaternion(self, quaternion):
        """
        Converts quaternion (w in last place) to euler roll, pitch, yaw
        quaternion = [x, y, z, w]
        Bellow should be replaced when porting for ROS 2 Python tf_conversions is done.
        """
        x = quaternion[0]
        y = quaternion[1]
        z = quaternion[2]
        w = quaternion[3]

        sinr_cosp = 2 * (w * x + y * z)
        cosr_cosp = 1 - 2 * (x * x + y * y)
        roll = np.arctan2(sinr_cosp, cosr_cosp)

        sinp = 2 * (w * y - z * x)
        pitch = np.arcsin(sinp)

        siny_cosp = 2 * (w * z + x * y)
        cosy_cosp = 1 - 2 * (y * y + z * z)
        yaw = np.arctan2(siny_cosp, cosy_cosp)

        return roll, pitch, yaw

    def init_movement_state(self):
        self.movement_state = "GOFOWARDS_1"
        self.init_turn_yaw = 0.0
        self.cmd = Twist()

    def elevator_up(self):
        self.up = Empty()
        self.elevator_pub.publish(self.up)

    def get_front_laser(self):
        return self.laser_msg.ranges[int(len(self.laser_msg.ranges)/2)]

    def get_yaw(self):
        return self.yaw

    def check_wall_detected(self):
        """
        We check if we are at less than max_dist of an object infront
        """
        result = self.get_front_laser() < self._min_dist_wall
        self.get_logger().info("check_wall_detected:"+str(result)+",dist=" +
                               str(self.get_front_laser())+",min="+str(self._min_dist_wall))
        return result

    def stop_robot(self, index):
        self.get_logger().info("STOP="+str(index))
        self.cmd.linear.x = 0.0
        self.cmd.angular.z = 0.0
        self.vel_pub.publish(self.cmd)

    def move_straight(self):
        self.get_logger().info("MOVE STRAIGHT")
        self.cmd.linear.x = 0.3
        self.cmd.angular.z = 0.0
        self.vel_pub.publish(self.cmd)

    def rotate(self, dir):
        self.get_logger().info("MOVE ROTATE")
        if dir == "left":
            self.cmd.angular.z = 0.2
        else:
            self.cmd.angular.z = -0.2
        self.cmd.linear.x = 0.0
        self.vel_pub.publish(self.cmd)

    def check_turn(self, desired_angle):
        """
        We chekc the init yaw with the current yaw , how much turn has been done
        we evaluate in abs angle
        """
        delta = self.init_turn_yaw - self.get_yaw()
        result = abs(delta) > abs(desired_angle)
        self.get_logger().info("Init YAW="+str(self.init_turn_yaw))
        self.get_logger().info("Current YAW="+str(self.get_yaw()))
        self.get_logger().info("Delta YAW="+str(self.init_turn_yaw))
        self.get_logger().info("RESULT YAW="+str(result))

        return result

    def move_loop(self):
        self.get_logger().info("Move Loop - STATE:"+str(self.movement_state))

        # We decide what to do based on state and scane and odom values:

        if self.movement_state == "START":
            # We check if teh distance is bigger than the min distance to wall
            if self.check_wall_detected():
                # We dont move or change state
                self.stop_robot(1)
            else:
                # We dont see wall, then go fowards
                self.movement_state = "GOFOWARDS_1"
        else:
            if self.movement_state == "GOFOWARDS_1":
                # We look for a wall
                if self.check_wall_detected():
                    # We stop We change to move right state
                    self.stop_robot(2)
                    self.movement_state = "TURN_RIGHT"
                    self.init_turn_yaw = self.get_yaw()
                else:
                    # We dont see wall, then go fowards
                    self.move_straight()
            else:
                # We are turning RIGHT
                if self.movement_state == "TURN_RIGHT":
                    # We have to check if we have turned 90 degrees ( 1.57 rad)
                    if self.check_turn(1.57):
                        self.stop_robot(3)
                        self.movement_state = "GOFOWARDS_2"
                    else:
                        # Continue turning
                        self.rotate("right")

                else:
                    if self.movement_state == "GOFOWARDS_2":
                        # We look for a wall
                        if self.check_wall_detected():
                            # We stop We change to move right state
                            self.stop_robot(4)
                            self.elevator_up()
                            self.movement_state = "END"
                        else:
                            # We dont see wall, then go fowards
                            self.move_straight()
                    else:
                        self.get_logger().info("ENDED MOVEMENT")

        self.get_logger().info("ENDED Loop")


def main(args=None):
    rclpy.init(args=args)

    try:
        my_action_server = MyActionServer()

        executor = MultiThreadedExecutor(num_threads=3)
        executor.add_node(my_action_server)

        try:
            executor.spin()
        finally:
            executor.shutdown()
            my_action_server.destroy_node()

    finally:
        rclpy.shutdown()


if __name__ == '__main__':
    main()
