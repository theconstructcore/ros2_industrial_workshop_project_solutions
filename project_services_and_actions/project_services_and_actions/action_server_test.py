import rclpy
from rclpy.action import ActionServer
from rclpy.node import Node

from project_custom_interfaces.action import GoToLoadingWithF

from geometry_msgs.msg import Twist
import time


class MyActionServer(Node):

    def __init__(self):
        super().__init__('my_action_server')
        self._action_server = ActionServer(
            self, GoToLoadingWithF, 'my_moving_robot_as', self.execute_callback)

        self.get_logger().info('SERVER ACTION READY...')

    def execute_callback(self, goal_handle):

        self.get_logger().info('Executing goal...')

        feedback_msg = GoToLoadingWithF.Feedback()
        feedback_msg.status = "Testing...."
        feedback_msg.distance_to_wall = 1.23456789

        for i in range(5):

            self.get_logger().info('Feedback: '+str(feedback_msg.status) +
                                   ",distance="+str(feedback_msg.distance_to_wall))

            goal_handle.publish_feedback(feedback_msg)

            time.sleep(1)

        goal_handle.succeed()

        feedback_msg.status = "ENDED...."
        feedback_msg.distance_to_wall = 0.0
        result = GoToLoadingWithF.Result()
        result.complete = True
        return result


def main(args=None):
    rclpy.init(args=args)

    my_action_server = MyActionServer()

    rclpy.spin(my_action_server)


if __name__ == '__main__':
    main()
