from launch import LaunchDescription
from launch_ros.actions import Node


def generate_launch_description():
    return LaunchDescription([
        Node(
            package='project_services_and_actions',
            executable='rb1_move_to_pick_location_service',
            output='screen',
            emulate_tty=True),
    ])
