from launch import LaunchDescription
from launch_ros.actions import Node


def generate_launch_description():
    return LaunchDescription([
        Node(
            package='project_services_and_actions',
            executable='action_server_v2_exe',
            output='screen',
            emulate_tty=True),
    ])
