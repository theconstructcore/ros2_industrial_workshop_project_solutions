from launch import LaunchDescription
from launch_ros.actions import Node


def generate_launch_description():
    return LaunchDescription([
        Node(
            package='project_topics_pkg',
            executable='rb1_move_to_pick_location',
            output='screen',
            emulate_tty=True),
    ])
